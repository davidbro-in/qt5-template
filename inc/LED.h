/****************************************************************************
*	Avoid multiple inclusion - begin										*
*****************************************************************************/
#ifndef _LED_H_
#define _LED_H_

/**
 * @file LED.h
 * @author David Broin <davidmbroin@gmail.com>
 * @date 2020/07/12
 * @brief File containing LED functions
 * @copyright All rights reserved.
 * @copyright @ref [MIT LICENSE](LICENSE.txt)
 */

/****************************************************************************
*	Inclusions of public function dependencies								*
*****************************************************************************/

#include "sapi_gpio.h"

/****************************************************************************
*	C++ - begin																*
*****************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************************************
*	Definition macros of public constants									*
*****************************************************************************/

/****************************************************************************
*	Public function-like macros												*
*****************************************************************************/

/****************************************************************************
*	Definitions of public data types										*
*****************************************************************************/

/**
 * @brief LED class members
 */
typedef struct{
	gpioMap_t gpio;
} LED_t;

/****************************************************************************
*	Prototypes (declarations) of public functions							*
*****************************************************************************/

/**
 * @brief Toggle LED status
 * @param led points to the led to toggle
 */
void toggle(const LED_t *led);

/**
 * @brief Turn LED on
 * @param led points to the led to turn on
 */
void on(const LED_t *led);

/**
 * @brief Turn LED off
 * @param led points to the led to turn off
 */
void off(const LED_t *led);

/****************************************************************************
*	Prototypes (declarations) of public interrupt functions					*
*****************************************************************************/

/****************************************************************************
*	C++ - end																*
*****************************************************************************/

#ifdef __cplusplus
}
#endif

/****************************************************************************
*	Avoid multiple inclusion - end											*
*****************************************************************************/

#endif /* _LED_H_ */

