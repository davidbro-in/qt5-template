/*==================[typedef]================================================*/

/* Functional states */
#ifndef ON
#define ON     1
#endif
#ifndef OFF
#define OFF    0
#endif

/* Define Boolean Data Type */
typedef uint8_t bool_t;
