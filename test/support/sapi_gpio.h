/* \file test/sapi_gpio.c */
#ifndef _SAPI_GPIO_H_
#define _SAPI_GPIO_H_

#include <stdint.h>
#include "sapi_datatypes.h"
#include "sapi_peripheral_map.h"

bool_t gpioWrite( gpioMap_t pin, bool_t value );
bool_t gpioToggle( gpioMap_t pin );

#endif
